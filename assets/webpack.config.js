const webpack = require('webpack');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');

module.exports = (env) => {
    const devMode = env === 'development';
    return {
        mode: devMode ? 'development' : 'production',
        devtool: devMode ? 'eval' : 'source-maps',
        entry: {
            app: './src/app.js'
        },
        output: {
            filename: devMode ? 'js/[name].js' : 'js/[name].[contenthash].js',
            path: path.resolve(__dirname, '../public/assets'),
        },
        module: {
            rules: [
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        // Loader to extract an minify CSS files
                        MiniCssExtractPlugin.loader,
                        // Interprets `@import` and `url()` like `import/require()` and will resolve them
                        {
                            loader: 'css-loader',
                            options: { sourceMap: true },
                        },
                        // Loader for webpack to process CSS with PostCSS
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        require('autoprefixer')
                                    ];
                                },
                                sourceMap: true,
                            }
                        },
                        // Loads a SASS/SCSS file and compiles it to CSS
                        {
                            loader: 'sass-loader',
                            options: { sourceMap: true },
                        },
                    ],
                },
                {
                    test: /\.(svg|eot|woff|woff2|ttf)$/,
                    loader: 'file-loader',
                    options: {
                        name: devMode ? 'webfonts/[name].[ext]' : 'webfonts/[name].[contenthash].[ext]',
                        publicPath: '/assets',
                    },
                },
                {
                    test: require.resolve('jquery'),
                    use: [
                        {loader: 'expose-loader', options: 'jQuery'},
                        {loader: 'expose-loader', options: '$'},
                    ],
                },
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify(devMode ? 'development' : 'production'),
                'process.env.DEBUG': JSON.stringify(devMode)
            }),
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: ['**/*', '!.gitignore', '!.htaccess'],
                protectWebpackAssets: devMode,
            }),
            new webpack.ProvidePlugin({
                'jQuery': 'jquery',
                '$': 'jquery',
            }),
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: devMode ? 'css/[name].css' : 'css/[name].[contenthash].css',
            }),
            new WebpackAssetsManifest({
                output: 'assets.json',
            }),
        ],
        performance: {
            assetFilter: function(assetFilename) {
                return assetFilename.endsWith('.js');
            },
            maxEntrypointSize: 500000,
            maxAssetSize: 500000,
        },
        optimization: {
            splitChunks: {
                chunks: 'all',
            },
        },
    };
};