// load jQuery and popper.js
import "jquery"
import "popper.js"

// load Bootstrap
import "bootstrap/js/src/index"

// load all CSS
import "./app.scss";
import bsCustomFileInput from "bs-custom-file-input";

// load custom modules
import "./album/album"

// globally initialize popovers and file uploads
$(document).ready(function() {
    $('.trigger-popover').popover({
        container: 'body'
    })
    bsCustomFileInput.init();
})

// make jQuery globally available
window.$ = window.jQuery = $;
