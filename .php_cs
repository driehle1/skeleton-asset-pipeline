<?php

$finder = PhpCsFixer\Finder::create()
    ->files()
    ->name('*.php')
    ->name('*.php.dist')
    ->name('*.phtml')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true)
    ->in(__DIR__ . '/bin/')
    ->in(__DIR__ . '/config/')
    ->in(__DIR__ . '/module/Application/')
    ->in(__DIR__ . '/public/');

return PhpCsFixer\Config::create()
    ->setUsingCache(true)
    ->setCacheFile(__DIR__ . '/.php_cs.cache')
    ->setRules([
        '@PSR2' => true,
        '@Symfony' => true,
        '@DoctrineAnnotation' => true,
        'align_multiline_comment' => ['comment_type' => 'phpdocs_like'],
        'concat_space' => ['spacing' => 'one'],
        'linebreak_after_opening_tag' => true,
        'no_short_echo_tag' => true,
        'no_superfluous_elseif' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'phpdoc_align' => ['align' => 'left'],
        'phpdoc_order' => true,
        'phpdoc_separation' => false,
        'ternary_to_null_coalescing' => true,
    ])
    ->setFinder($finder);
