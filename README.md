Template for an Asset Pipeline
------------------------------

This sample project shows a Webpack-based asset pipeline. To run this sample you will need Docker and docker-compose. Simply start all containers:

```
docker-compose up -d
```

There is a container running `node` where you will need to install all required node packages, see `assets/packages.json`. You will need to run `npm install` inside of that container. To do you, execute:

```
docker-compose run --rm node npm install
```

To keep things simple you can use `bin/npm.bat` (on Windows) or `bin/npm.sh` (on Linux) as a shortcut.
